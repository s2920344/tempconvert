package nl.utwente.di.tempConvert;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

public class TempConvert extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private Converter converter;
	
    public void init() throws ServletException {
    	converter = new Converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Temp Conversion";

    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   converter.celsiusToFahrenheit(Double.parseDouble(request.getParameter("celsius"))) +
                "</BODY></HTML>");
  }
}
