package nl.utwente.di.tempConvert;

public class Converter {
    public double celsiusToFahrenheit(double temp) {
        return temp * 9 / 5 + 32;
    }
}
